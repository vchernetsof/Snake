//
// Created by viktor on 4/27/18.
//

#ifndef SNAKE_SNAKE_H
#define SNAKE_SNAKE_H

#include <string>
#include <iostream>
#include <vector>
#include <bits/shared_ptr.h>
#include "Food.h"
#include "Snake.h"


class Snake;

class SnakeField {
private:
    enum {DIRECTION_LEFT = 1, DIRECTION_RIGHT = 2, DIRECTION_UP = 3, DIRECTION_DOWN = 4};

    const int MAP_WIDTH = 40;
    const int MAP_HEIGHT = 15;
    const char SNAKE_BODY = '*';
    const char FOODS[5] = {'%', '$', '&', '@', '+'};
public:
    SnakeField();

    bool isMapBorder(int, int);

    bool isFood(int, int);

    bool ateFood();

//    bool isOutBorder();
    void draw();
    bool isSnakeBody(int, int);
};

#endif //SNAKE_SNAKE_H
