//
// Created by viktor on 4/27/18.
//

#ifndef SNAKE_SNAKEBODY_H
#define SNAKE_SNAKEBODY_H

class SnakeBody {
private:
    int x;
    int y;
public:
    SnakeBody();

    void setX(int _x);

    int getX();

    void setY(int _y);

    int getY();
};

#endif //SNAKE_SNAKEBODY_H
