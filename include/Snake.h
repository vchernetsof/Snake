//
// Created by viktor on 4/27/18.
//

#ifndef SNAKE_SNAKE_H
#define SNAKE_SNAKE_H

#include <vector>
#include "../include/SnakeBody.h"

class Snake {
private:
    std::vector<SnakeBody*> snake;
    const int SNAKE_SIZE = 5;
    SnakeBody *head;
public:
    Snake();

    bool isItselfEat();

    bool isOverlapsBody(int, int);
};

#endif //SNAKE_SNAKE_H
