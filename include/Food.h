//
// Created by viktor on 4/27/18.
//

#ifndef SNAKE_FOOD_H
#define SNAKE_FOOD_H

class Food {
private:
    int x;
    int y;
public:
    explicit Food(int = 0, int = 0);

    int getX();

    void setX(int);

    int getY();

    void setY(int);
};

#endif //SNAKE_FOOD_H
