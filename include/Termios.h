//
// Created by viktor on 4/27/18.
//

#ifndef SNAKE_TERMIOS_H
#define SNAKE_TERMIOS_H


#include <termios.h>

class Termios {
private:
    termios terminalSettings;
    termios newTerminalSettings;

public:
    Termios();
    ~Termios();
};

#endif //SNAKE_TERMIOS_H
