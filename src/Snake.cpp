//
// Created by viktor on 4/27/18.
//

#include "../include/Snake.h"

Snake::Snake() {
    for (auto bodySize = 0; bodySize < SNAKE_SIZE; ++bodySize) {
        auto *snakeBody = new SnakeBody();

        snakeBody->setX(-10);
        snakeBody->setY(-10);

        snake.push_back(snakeBody);
    }

    snake.at(0)->setX(5);
    snake.at(0)->setY(3);

    head = snake.at(0);
}

bool Snake::isItselfEat() {
    for (auto snakeBody : snake) {
        if (snakeBody->getX() == head->getX() && snakeBody->getY() == head->getY()) {
            return true;
        }
    }

    return false;
}

bool Snake::isOverlapsBody(int x, int y) {
    for (auto snakeBody : snake) {
        if (snakeBody->getY() == y && snakeBody->getX() == x) {
            return true;
        }
    }

    return false;
}
