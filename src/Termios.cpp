//
// Created by viktor on 4/27/18.
//

#include <unistd.h>
#include "../include/Termios.h"

Termios::Termios() {
    tcgetattr(STDIN_FILENO, &terminalSettings);

    newTerminalSettings = terminalSettings;

    newTerminalSettings.c_lflag &= ~( ICANON | ECHO );
    newTerminalSettings.c_cc[VTIME] = 0;
    newTerminalSettings.c_cc[VMIN] = 1;

    tcsetattr(STDIN_FILENO, TCSANOW, &newTerminalSettings);
}

Termios::~Termios() {
    tcsetattr(STDIN_FILENO, TCSANOW, &terminalSettings);
}