//
// Created by viktor on 4/27/18.
//

#include "../include/SnakeBody.h"

SnakeBody::SnakeBody() {
    x = 0;
    y = 0;
}

int SnakeBody::getX() {
    return x;
}

void SnakeBody::setX(int _x) {
    x = _x;
}

int SnakeBody::getY() {
    return y;
}

void SnakeBody::setY(int _y) {
    y = _y;
}