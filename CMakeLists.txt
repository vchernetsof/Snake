cmake_minimum_required(VERSION 3.10)
project(Snake)

set(CMAKE_CXX_STANDARD 17)

add_executable(Snake main.cpp include/SnakeField.h src/SnakeField.cpp include/Termios.h src/Termios.cpp include/Food.h include/SnakeBody.h src/SnakeBody.cpp include/Snake.h src/Snake.cpp src/Food.cpp)